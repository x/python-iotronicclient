# The order of packages is significant, because pip processes them in the order
# of appearance. Changing the order has an impact on the overall integration
# process, which may cause wedges in the gate later.
pbr>=2.0.0 # Apache-2.0
appdirs>=1.3.0 # MIT License
dogpile.cache>=0.6.2 # BSD
#jsonschema!=2.5.0,<3.0.0,>=2.0.0 # MIT
jsonschema>=3.2.0 # MIT
keystoneauth1>=2.18.0 # Apache-2.0
osc-lib>=1.2.0 # Apache-2.0
#oslo.i18n>=2.1.0 # Apache-2.0
oslo.i18n>=3.15.3  # Apache-2.0
oslo.serialization>=1.10.0 # Apache-2.0
#oslo.utils>=3.20.0 # Apache-2.0
oslo.utils>=3.33.0,!=3.39.1,!=3.40.0,!=3.40.1  # Apache-2.0
#PrettyTable<0.8,>=0.7.1 # BSD
PrettyTable>=2.4.0 
python-openstackclient>=3.3.0 # Apache-2.0
PyYAML>=3.10.0 # MIT
requests!=2.24.0,!=2.20.0,>=2.10.0 # Apache-2.0
six>=1.9.0 # MIT
